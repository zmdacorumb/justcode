import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import Helper from '../../lib/helper';
import Sound from 'react-native-sound';
//TouchableOpacity當用戶點擊時，它將更改其透明度
//PropTypes 是屬性類型常量
//Sound 使我們能在應用程式中播放聲音檔

export default WordDefinition = ({def}) => {
  const [loadingMp3, setLoadingMp3] = useState(false);

  let word = Helper.carefullyGetValue(def, ['word']);

  if (word.length > 0) {
    word = Helper.capitalize(word);
    word = '[' + word + ']';
  } else {
    word = 'Definition not found.';
  }
  // 嘗試獲取MP3路徑
  let speakMp3 = Helper.carefullyGetValue(def, [
    'results',
    '0',
    'lexicalEntries',
    '0',
    'entries',
    '0',
    'pronunciations',
    '0',
    'audioFile',
  ]);
  console.log('speakMp3', speakMp3);
  return (
    <>
      {def && (
        <View style={styles.content}>
          <View style={styles.row}>
            <Text style={styles.word}>{word}</Text>
            {speakMp3.length > 0 && (
              <View>
                {loadingMp3 ? (
                  <ActivityIndicator
                    size="large"
                    color="#219bd9"
                    style={{marginLeft: 10}}
                  />
                ) : (
                  <TouchableOpacity
                    onPress={() => playWord(speakMp3, setLoadingMp3)}>
                    <Text style={styles.speaker}>{'🔈'}</Text>
                  </TouchableOpacity>
                )}
              </View>
            )}
          </View>
          {/* 調用 getLexicalEnteries()，來獲取單詞定義的詞法(lexical)條目 */}
          <View>{getLexicalEntries(def)}</View>
        </View>
      )}
    </>
  );
};
// speakMp3 會是一段url
const playWord = (speakMp3, setLoadingMp3) => {
  setLoadingMp3(true);
  console.log('playing', speakMp3);
  // Enable playback in silence mode
  Sound.setCategory('Playback'); //設置類別為播放，這樣如果設備處於靜音狀態，仍會播放mp3.

  // See notes below about preloading sounds within initialization code below.
  // 創建Sound對象，該對象帶有mp3路徑，和一個回調函數，回調函數中檢查是否有任何錯誤
  let player = new Sound(speakMp3, null, (error) => {
    if (error) {
      console.log('failed to load the sound', error);
      setLoadingMp3(false);
      return;
    }

    player.setVolume(1); //設置音量為最大

    //play the sound with an onEnd callback
    // 調用播放功能來播放mp3
    player.play((success) => {
      if (success) {
        console.log('successsfully finished playing', success);
      } else {
        console.log('playback failed due to audio decoding errors');
      }

      //播放完畢後，將loadingMp3設為否定
      setLoadingMp3(false);
      // 釋放播放器對象
      player.release();
    });
  });
};

const getLexicalEntries = (def) => {
  const jsxEntries = []; //用來保存所有jsx元素
  var lexicalEntries = null;

  //檢查是否有任何詞法條目, 如有就遍歷每個條目並創建界面元素，push到jsxEntries陣列中
  if (Helper.isNotNullAndUndefined(def, ['results', '0', 'lexicalEntries'])) {
    lexicalEntries = def.results[0].lexicalEntries;

    lexicalEntries.forEach((lexicalItem, index) => {
      jsxEntries.push(
        <View key={'lexicalEntry_' + index} style={styles.lexicalContent}>
          <Text style={styles.category}>
            {Helper.carefullyGetValue(
              lexicalItem,
              ['lexicalCategory', 'text'],
              '',
            )}
          </Text>
          {/* 調用 getSenses()，來顯示單詞定義和單詞用法示例*/}
          {Helper.isNotNullAndUndefined(lexicalItem, [
            'entries',
            '0',
            'senses',
          ]) && getSenses(lexicalItem.entries[0].senses)}
        </View>,
      );
    });
  }
  console.log('jsxEntries', jsxEntries);
  return <>{jsxEntries}</>;
};
// senses = 單詞定義
const getSenses = (senses) => {
  const jsxSenses = [];

  //先檢查是否有任何單詞定義(senses), 如有則遍歷每個單詞定義，並獲取該詞的用法示例
  if (senses && senses.length > 0) {
    senses.forEach((sense, index) => {
      let example = Helper.carefullyGetValue(
        sense,
        ['examples', '0', 'text'],
        '',
      );
      if (example.length > 0) example = 'E.g. ' + example;

      //如單詞定義存在，則創建單詞界面以顯示單詞定義和用法示例界面元素
      if (sense.definitions) {
        jsxSenses.push(
          <View style={styles.row} key={'sense_ ' + index}>
            <Text style={styles.bullet}>{'\u2022'}</Text>
            <View style={styles.column}>
              <Text style={styles.definition}>
                {Helper.carefullyGetValue(sense, ['definitions', '0'], '')}
              </Text>
              <Text style={styles.example}>{example}</Text>
            </View>
          </View>,
        );
      }
    });
  }
  return <View style={{marginLeft: 10}}>{jsxSenses}</View>;
};

//將組件屬性的類型，定義為物件
WordDefinition.propTypes = {
  def: PropTypes.object,
};

WordDefinition.defaultProps = {
  def: null,
};

const styles = StyleSheet.create({
  content: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  word: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  speaker: {
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  lexicalContent: {
    paddingTop: 20,
  },
  category: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  column: {
    flex: 1,
    flexDirection: 'column',
  },
  bullet: {
    maxWidth: 20,
    minWidth: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
  definition: {
    fontSize: 18,
    fontWeight: 'normal',
  },
  example: {
    fontSize: 14,
    fontWeight: 'normal',
    color: '#999999',
    marginBottom: 10,
  },
});
