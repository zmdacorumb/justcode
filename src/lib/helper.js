//這是小助理函數庫，包含應用程序所需要的一些靜態函數，使用靜態函數而不是普通函數，是因不需用Heplper對象來使用它。

export default class Helper {
  //用來檢查指定對象(obj)是否存在指定的屬性
  static isNotNullAndUndefined(obj, props = []) {
    let bIsNullorUndefined = obj === null || obj === undefined;

    let curObj = null;

    if (!bIsNullorUndefined) {
      curObj = obj;
      if (props !== null) {
        for (let idx = 0; idx < props.length; idx++) {
          bIsNullorUndefined =
            curObj[props[idx]] === null || curObj[props[idx]] === undefined;
          curObj = curObj[props[idx]];

          if (bIsNullorUndefined) break;
        }
      }
    }
    return !bIsNullorUndefined;
  }

  //將返回特定屬性的值，如果指定的屬性不存在，它將返回由defaultValue參數傳入的默認值
  static carefullyGetValue(obj, props = [], defaultValue = '') {
    let bIsNullorUndefined = obj === null || obj === undefined;
    let curObj = null;
    if (!bIsNullorUndefined) {
      curObj = obj;

      if (props !== null) {
        for (let idx = 0; idx < props.length; idx++) {
          bIsNullorUndefined =
            curObj[props[idx]] === null || curObj[props[idx]] === undefined;
          curObj = curObj[props[idx]]; // Set the curObj[props[idx]] to curObj so that it will recursive down the depth of the object
          // console.log('curObj', curObj);

          if (bIsNullorUndefined) break;
        }
      }
    }

    if (bIsNullorUndefined) return defaultValue;
    else return curObj;
  }

  //將指定的字符串的第一個字母變成大寫
  static capitalize(str) {
    if (typeof str !== 'string') return '';
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
}
