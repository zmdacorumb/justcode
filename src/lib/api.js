import Helper from './helper';

const baseURL = 'https://od-api.oxforddictionaries.com/api/v2';
const oxfordAuth = {
  app_id: 'ffb82d29',
  app_key: 'f7ee8ba3fe72595c9e2bdb730158f937',
};

export default class Api {
  //將請求發送至對方服務器
  static apiRequest(methodName, method = 'GET', body = null) {
    if (baseURL.length > 0) {
      // If requestURL is speicfied, then use it else use the serverURL,
      // 如果指定了requestURL，则使用它，否则使用serverURL
      let apiURL = baseURL;
      // If the last character of the apiURL is '/', remove it. 如果apiURL的最后一个字符是'/'，请将其删除。
      apiURL =
        apiURL[apiURL.length - 1] == '/'
          ? apiURL.substring(0, apiURL.length - 1)
          : apiURL;
      return fetch(apiURL + '/' + methodName, {
        method: method,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          app_id: oxfordAuth.app_id,
          app_key: oxfordAuth.app_key,
        },
        body:
          method == 'post' ? (body !== null ? JSON.stringify(body) : '') : '',
      });
    } else {
      return new Promise((resolve, reject) => {
        reject({
          success: false,
          statusCode: 'ERROR',
          message: 'Base URL is not set',
          payload: null,
        });
      });
    }
  }
  //檢查對方服務器返回的代碼
  static checkHttpResponse(response) {
    // 如果有回傳值
    if (response !== null && response !== undefined) {
      if (response.ok) {
        // console.log('checkHttpResponse: ', response.json());
        // console.log('response.json()', response.json());
        return response.json();
      } else {
        return {
          succes: false,
          statusCode: 'HTTP_' + response.status,
          message: Helper.isNotNuLLAndUndefined(response, statusText)
            ? response.statusText
            : 'Server error HTTP' + response.status,
          payload: null,
        };
      }
    }
    // 如果沒回傳值
    else {
      return {
        success: false,
        statusCode: 'HTTP_NORESPONSE',
        message: 'No respnse from the request',
        payload: null,
      };
    }
  }
  //返回單詞的首詞(例用戶輸入Stars, 首詞為Star),如用Stars查定義，而不是star查找定義，那牛津辭典api將找不到任何定義。
  static getLemmas(word) {
    return new Promise((resolve, reject) => {
      this.apiRequest('lemmas/en/' + word.toLowerCase()) //toLowerCase()  將字符串轉換為英文小寫
        .then((response) => this.checkHttpResponse(response)) //用checkHttpResponse()，來檢查返回結果的狀態，以確保它是成功的API請求，如果checkHttpResponse()失敗，將觸發catch函數，如果成功將觸發下個then函數,每個then都會返回一個Promise.
        .then((responseJson) => {
          console.log('Response JSON:', responseJson);
          resolve({
            success: true,
            statusCode: 'HTTP_200',
            message: '',
            payload: responseJson,
          });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  //此函數接受'首詞"並從牛津辭典API返回其定義。
  static getDefinition(word) {
    return new Promise((resolve, reject) => {
      this.apiRequest('entries/en/' + word.toLowerCase())
        .then((response) => this.checkHttpResponse(response))
        .then((responseJson) => {
          resolve({
            success: true,
            statusCode: 'HTTP_200',
            message: '',
            payload: responseJson,
          });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
