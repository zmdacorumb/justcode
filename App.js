/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Api from './src/lib/api';
import Helper from './src/lib/helper';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  Button,
  ActivityIndicator,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// import WordDefinition from './src/components/wordDef';
import WordDefinition from './src/components/wordDef';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {userWord: '', errorMsg: '', loading: false, definition: null};
    console.log('App started...');
  }

  onUserWordChange(text) {
    this.setState({userWord: text});
    console.log('User Input: ', text);
  }

  async onSearch() {
    // 1. 檢查用戶有無輸入，無輸入則return離開onSearch()
    if (this.state.userWord.length <= 0) {
      this.setState({errorMsg: 'Please specify the word to lookup.'});
      return;
    }
    try {
      // 2. 打開ActivityIndicator loading動畫效果
      this.setState({loading: true});

      // 3. 調用getLemmas()函數，並將返回值傳給lemmas.
      let lemmas = await Api.getLemmas(this.state.userWord);
      console.log('Lemmas:', lemmas);

      // 4.檢查getLemmas()有無運行成功
      if (lemmas.success) {
        //5. 執行carefullyGetValue()，以讀取牛津辭典API返回的JSON結果中的首詞,若該詞有首詞，將返回值傳給headWord.
        let headWord = Helper.carefullyGetValue(
          lemmas,
          [
            'payload',
            'results',
            '0',
            'lexicalEntries',
            '0',
            'inflectionOf',
            '0',
            'id',
          ],
          '',
        );
        console.log('Headword is:', headWord);
        //6. headWord存在時，調用getDefinition()，來檢查牛津辭典有無該首詞。
        if (headWord.length > 0) {
          let wordDefinition = await Api.getDefinition(headWord);
          //7. 若getDefinition()檢查有無該首詞，並將結果傳給wordDefinition，wordDefinition的值為有效時，將wordDefinition設置到definition。
          if (wordDefinition.success) {
            this.setState({
              errorMsg: '',
              loading: false,
              definition: wordDefinition.payload,
            });
            console.log('Word Definition: ', wordDefinition.payload);
          }
          //8. 若無該首詞則顯示，無法從牛津獲得結果
          else {
            this.setState({
              errorMsg: 'Unable to get result from Oxford: ' + lemmas.message,
              loading: false,
              definition: null,
            });
          }
        }
        //9. 無效的字詞。 請指定一個有效的單詞
        else {
          this.setState({
            errorMsg: 'Invalid word. Please specify a valid word',
            loading: false,
            definition: null,
          });
        }
      }
      // 無法從牛津獲得結果
      else {
        this.setState({
          errorMsg: 'Unable to get result from Oxford: ' + lemmas.message,
          loading: false,
          definition: null,
        });
      }
    } catch (err) {
      console.log('Error:', err);
      this.setState({loading: false, errorMsg: 'not search'});
    }
  }
  //   onSearch() {
  //     if (this.state.userWord.length <= 0) {
  //       this.setState({errorMsg: 'Please specify the word to lookup.'});
  //       return;
  //     }
  //     try {
  //       this.setState({loading: true});

  //       Api.getLemmas(this.state.userWord)
  //         .then((result) => {
  //           console.log('Result:', result);
  //           this.setState({loading: false});
  //         })
  //         .catch((err) => {
  //           console.log('Error', err);
  //           this.setState({loading: false});
  //         });
  //     } catch (error) {
  //       console.log('Error: ', error);
  //       this.setState({
  //         loading: false,
  //         errorMsg: error.message,
  //         definition: null,
  //       });
  //     }
  // }
  render() {
    console.log('definition', this.state.definition);
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            {/* <Header />
            {global.HermesInternal == null ? null : (
              <View style={styles.engine}>
                <Text style={styles.footer}>Engine: Hermes</Text>
              </View>
            )} */}
            <View style={[styles.colunm, styles.header]}>
              <Image
                style={styles.logo}
                source={require('./assets/icon.png')}></Image>
            </View>
            <TextInput
              style={{
                height: 40,
                borderColor: 'gray',
                borderWidth: 1,
                paddingLeft: 4,
                paddingRight: 4,
              }}
              onChangeText={(text) => this.onUserWordChange(text)}
              placeholder={'Key in the word to search'}
              value={this.state.userWord}
            />

            <View style={{minHeight: 10, maxHeight: 10}}></View>

            <Button title="Search" onPress={() => this.onSearch()} />

            {this.state.errorMsg.length > 0 && (
              <Text style={styles.errorMsg}>{this.state.errorMsg}</Text>
            )}

            {/* 用戶點擊search按鈕後，顯視單詞定義，用下面另一個component顯示 */}
            <WordDefinition def={this.state.definition} />
          </ScrollView>
        </SafeAreaView>
        {/* 當用戶點擊search按鈕時，使用react natived 的 ActivityIndicator 組件顯視加載螢幕  */}
        {this.state.loading && (
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color={'#219bd9'}
          />
        )}
      </>
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#219bd930',
    color: '#ff0000',
  },
  scrollView: {
    // backgroundColor: Colors.lighter,
    padding: 6,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  header: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  colunm: {
    flex: 1,
    // flexDirection: 'column',
  },
  logo: {
    width: 100,
    height: 100,
  },
  errorMsg: {
    fontSize: 18,
    fontWeight: '400',
    color: 'red',
  },

  // engine: {
  //   position: 'absolute',
  //   right: 0,
  // },
  // body: {
  //   backgroundColor: Colors.white,
  // },
  // sectionContainer: {
  //   marginTop: 32,
  //   paddingHorizontal: 24,
  // },
  // sectionDescription: {
  //   marginTop: 8,
  //   fontSize: 18,
  //   fontWeight: '400',
  //   color: Colors.dark,
  // },
  // highlight: {
  //   fontWeight: '700',
  // },
  // footer: {
  //   color: Colors.dark,
  //   fontSize: 12,
  //   fontWeight: '600',
  //   padding: 4,
  //   paddingRight: 12,
  //   textAlign: 'right',
  // },
});

export default App;
